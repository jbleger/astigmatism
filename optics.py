# pylint: disable=missing-class-docstring,missing-function-docstring,missing-module-docstring,invalid-name,unnecessary-lambda-assignment
import sys
import os
import shutil

import matplotlib.pyplot as plt
import numpy as np

ftransf = lambda x: np.array([[1, 0.35, 0], [0, 0.35, 1]]) @ np.asarray(x).T


class Draw:
    def __init__(self, boundx=(-3, 3), boundy=2):
        self.fdist0 = 2
        self.fdist1 = 1.5  # 1.75
        self.angle = 10 / 180 * np.pi
        self.fig, self.ax = plt.subplots(
            frameon=False, figsize=(3 * (boundx[1] - boundx[0]) / 2, 3 * boundy)
        )
        self.fig.subplots_adjust(bottom=0)
        self.fig.subplots_adjust(top=1)
        self.fig.subplots_adjust(right=1)
        self.fig.subplots_adjust(left=0)

        self.ax.cla()
        for spine in self.ax.spines.values():
            spine.set_edgecolor('white')
        self.ax.set_xlim(boundx)
        self.ax.set_ylim(-boundy, boundy)
        self.ax.plot(*ftransf([[-3, 0, 0], [3, 0, 0]]), color="k", alpha=0.2)
        self.ax.plot(*ftransf([[0, -3, 0], [0, 3, 0]]), color="k", alpha=0.2)
        self.ax.plot(*ftransf([[0, 0, -3], [0, 0, 3]]), color="k", alpha=0.2)

    def draw_lentille(self, radius=0.5):
        lentille = np.array(
            [(0, np.cos(a), np.sin(a)) for a in np.linspace(0, 2 * np.pi, 10000)]
        )
        self.ax.fill(
            *ftransf(np.concatenate((lentille, radius * lentille[::-1]))), color="k"
        )

    def draw_filter(self, angle, x=-2, radius=0.5 + 1 / 6, sep=0.01):
        half_circle = np.array(
            [
                (x, np.cos(angle + a), np.sin(angle + a))
                for a in np.linspace(np.pi * sep, (1 - sep) * np.pi, 10000)
            ]
        )
        self.ax.fill(*ftransf(half_circle), color="k")
        self.ax.fill(*ftransf(half_circle * (1, -1, -1)), color="k")
        self.ax.fill(
            *ftransf(
                [
                    [
                        x,
                        radius * np.cos(angle + sep * np.pi),
                        radius * np.sin(angle + sep * np.pi),
                    ],
                    [
                        x,
                        radius * np.cos(angle + (1 - sep) * np.pi),
                        radius * np.sin(angle + (1 - sep) * np.pi),
                    ],
                    [
                        x,
                        -radius * np.cos(angle + sep * np.pi),
                        -radius * np.sin(angle + sep * np.pi),
                    ],
                    [
                        x,
                        -radius * np.cos(angle + (1 - sep) * np.pi),
                        -radius * np.sin(angle + (1 - sep) * np.pi),
                    ],
                ]
            ),
            color="y",
            linewidth=0,
            alpha=0.5,
        )

    def draw_circular_light(self, xmin=-4, xmax=-2, radius=0.5 + 1 / 6):
        for angle in np.linspace(0, np.pi, 51)[:-1]:
            self.ax.fill(
                *ftransf(
                    [
                        [xmin, radius * np.cos(angle), radius * np.sin(angle)],
                        [
                            xmin,
                            radius * np.cos(angle + np.pi),
                            radius * np.sin(angle + np.pi),
                        ],
                        [
                            xmax,
                            radius * np.cos(angle + np.pi),
                            radius * np.sin(angle + np.pi),
                        ],
                        [xmax, radius * np.cos(angle), radius * np.sin(angle)],
                    ]
                ),
                color="y",
                alpha=3 * 0.5 / 50,
                linewidth=0,
            )

    def draw_plane_light(self, angle, xmin=-2, xmax=0, radius=0.5 + 1 / 6):
        self.ax.fill(
            *ftransf(
                [
                    [xmin, radius * np.cos(angle), radius * np.sin(angle)],
                    [xmax, radius * np.cos(angle), radius * np.sin(angle)],
                    [
                        xmax,
                        radius * np.cos(angle + np.pi),
                        radius * np.sin(angle + np.pi),
                    ],
                    [
                        xmin,
                        radius * np.cos(angle + np.pi),
                        radius * np.sin(angle + np.pi),
                    ],
                ]
            ),
            color="y",
            alpha=0.5,
            linewidth=0,
        )

    def draw_free_focalization(self, angle, xmin=0, rxmax=3, radius=0.5):
        focal_dist = (
            np.cos(angle + self.angle) ** 2 * self.fdist0
            + np.sin(angle + self.angle) ** 2 * self.fdist1
        )

        p1 = ftransf([xmin, radius * np.cos(angle), radius * np.sin(angle)])
        p2 = ftransf(
            [xmin, radius * np.cos(angle + np.pi), radius * np.sin(angle + np.pi)]
        )
        p1b = (rxmax, -p1[1] / (focal_dist - p1[0]) * (rxmax - focal_dist))
        p2b = (rxmax, -p2[1] / (focal_dist - p2[0]) * (rxmax - focal_dist))
        self.ax.fill(*zip(p1, p1b, p2b, p2), color="y", alpha=0.5, linewidth=0)

    def draw_free_focalization_circular(self, xmin=0, rxmax=3, radius=0.5):
        for angle in np.linspace(0, np.pi, 51)[:-1]:
            focal_dist = (
                np.cos(angle + self.angle) ** 2 * self.fdist0
                + np.sin(angle + self.angle) ** 2 * self.fdist1
            )
            p1 = ftransf([xmin, radius * np.cos(angle), radius * np.sin(angle)])
            p2 = ftransf(
                [xmin, radius * np.cos(angle + np.pi), radius * np.sin(angle + np.pi)]
            )
            p1b = (rxmax, -p1[1] / (focal_dist - p1[0]) * (rxmax - focal_dist))
            p2b = (rxmax, -p2[1] / (focal_dist - p2[0]) * (rxmax - focal_dist))
            self.ax.fill(
                *zip(p1, p1b, p2b, p2), color="y", alpha=3 * 0.5 / 50, linewidth=0
            )

    def _calc_img_plane(self, angle, xmin, xmax, radius):
        focal_dist = (
            np.cos(angle + self.angle) ** 2 * self.fdist0
            + np.sin(angle + self.angle) ** 2 * self.fdist1
        )
        p1 = [xmin, radius * np.cos(angle), radius * np.sin(angle)]
        p2 = [xmin, radius * np.cos(angle + np.pi), radius * np.sin(angle + np.pi)]
        p1b = [
            xmax,
            -p1[1] / (focal_dist - p1[0]) * (xmax - focal_dist),
            -p1[2] / (focal_dist - p1[0]) * (xmax - focal_dist),
        ]
        p2b = [
            xmax,
            -p2[1] / (focal_dist - p2[0]) * (xmax - focal_dist),
            -p2[2] / (focal_dist - p2[0]) * (xmax - focal_dist),
        ]
        return p1, p2, p1b, p2b

    def draw_focalization_to_plane(self, angle, xmin=0, xmax=2, radius=0.5):
        p1, p2, p1b, p2b = self._calc_img_plane(angle, xmin, xmax, radius)
        self.ax.fill(*ftransf([p1, p1b, p2b, p2]), color="y", alpha=0.5, linewidth=0)

    def draw_focalization_to_plane_circular(self, xmin=0, xmax=2, radius=0.5):
        for angle in np.linspace(0, np.pi, 51)[:-1]:
            p1, p2, p1b, p2b = self._calc_img_plane(angle, xmin, xmax, radius)
            self.ax.fill(
                *ftransf([p1, p1b, p2b, p2]), color="y", alpha=3 * 0.5 / 50, linewidth=0
            )

    def draw_screen(  # pylint: disable=too-many-arguments
        self, angle, xmin=0, x=2, radius=0.5, size=0.16, xbig=4.5, sizebig=1
    ):
        self._screen(xmin, x, size, xbig, sizebig)
        _, _, p1b, p2b = self._calc_img_plane(angle, xmin, x, radius)
        self.ax.plot(*ftransf([p1b, p2b]), color="y", alpha=1, linewidth=2)

        self.ax.plot(
            *(np.array([xbig, 0]) + np.array([p1b[1:], p2b[1:]]) * sizebig / size).T,
            color="y",
            alpha=1,
            linewidth=sizebig / size,
        )

    def draw_screen_circular(  # pylint: disable=too-many-arguments
        self, xmin=0, x=2, radius=0.5, size=0.16, xbig=4.5, sizebig=1
    ):
        self._screen(xmin, x, size, xbig, sizebig)
        for angle in np.linspace(0, np.pi, 51)[:-1]:
            _, _, p1b, p2b = self._calc_img_plane(angle, xmin, x, radius)
            self.ax.plot(*ftransf([p1b, p2b]), color="y", alpha=3 / 50, linewidth=2)

        for angle in np.linspace(0, np.pi, 101)[:-1]:
            _, _, p1b, p2b = self._calc_img_plane(angle, xmin, x, radius)
            self.ax.plot(
                *(
                    np.array([xbig, 0]) + np.array([p1b[1:], p2b[1:]]) * sizebig / size
                ).T,
                color="y",
                alpha=2 / 50,
                linewidth=sizebig / size,
            )

    def _screen(self, _xmin, x, size, xbig, sizebig):
        frame = ftransf(
            [
                [x, -size, -size],
                [x, -size, +size],
                [x, +size, +size],
                [x, +size, -size],
                [x, -size, -size],
            ]
        )
        framebig = [
            [xbig - sizebig, -sizebig],
            [xbig - sizebig, +sizebig],
            [xbig + sizebig, +sizebig],
            [xbig + sizebig, -sizebig],
            [xbig - sizebig, -sizebig],
        ]

        self.ax.plot(*frame, color="r", alpha=0.2)
        self.ax.plot(*ftransf([[x, 0, -size], [x, 0, +size]]), color="k", alpha=0.2)
        self.ax.plot(*ftransf([[x, -size, 0], [x, +size, 0]]), color="k", alpha=0.2)
        self.ax.fill(*frame, color="k", alpha=0.1)

        self.ax.plot(*zip(*framebig), color="r", alpha=0.2)
        self.ax.plot(
            *zip(
                (xbig - sizebig, 0),
                (xbig + sizebig, 0),
            ),
            color="k",
            alpha=0.2,
        )
        self.ax.plot(
            *zip(
                (xbig, -sizebig),
                (xbig, sizebig),
            ),
            color="k",
            alpha=0.2,
        )
        self.ax.fill(*zip(*framebig), color="k", alpha=0.1)

        for pa, pb in zip(frame.T[(0, 2), :], framebig[:2]):
            self.ax.plot(*zip(pa, pb), color="r", alpha=0.1)


def draw_regular_free(filename):
    draw = Draw((-3, 3), 2)
    draw.fdist1 = draw.fdist0 = 1.875
    draw.draw_circular_light(xmax=0)
    draw.draw_lentille()
    draw.draw_free_focalization_circular()
    draw.fig.savefig(filename)


MOVE_SCREEN = np.concatenate(
    (np.linspace(1.75, 2, 51)[:-1], np.linspace(2, 1.75, 51)[:-1])
)
MOVE_SCREEN2 = np.concatenate(
    (np.linspace(1.5, 2, 51)[:-1], np.linspace(2, 1.5, 51)[:-1])
)


def draw_regular_moving_screen(dirname):
    shutil.rmtree(dirname, ignore_errors=True)
    os.mkdir(dirname)
    for i, screen in enumerate(MOVE_SCREEN):
        draw = Draw((-3, 6), 2)
        draw.fdist1 = draw.fdist0 = 1.875
        draw.draw_circular_light(xmax=0)
        draw.draw_lentille()
        draw.draw_focalization_to_plane_circular(xmax=screen)
        draw.draw_screen_circular(x=screen)
        draw.fig.savefig(f"{dirname}/{i:04d}.png")
        del draw
        plt.close()


MOVE_LIGHT = np.linspace(0, np.pi, 76)[:-1]


def draw_regular_moving_light_free(dirname):
    shutil.rmtree(dirname, ignore_errors=True)
    os.mkdir(dirname)
    for i, angle in enumerate(MOVE_LIGHT):
        draw = Draw((-3, 3), 2)
        draw.fdist1 = draw.fdist0 = 1.875
        draw.draw_circular_light()
        draw.draw_filter(angle)
        draw.draw_plane_light(angle)
        draw.draw_lentille()
        draw.draw_free_focalization(angle)
        draw.fig.savefig(f"{dirname}/{i:04d}.png")
        del draw
        plt.close()


def draw_asti_moving_light_free(dirname):
    shutil.rmtree(dirname, ignore_errors=True)
    os.mkdir(dirname)
    for i, angle in enumerate(MOVE_LIGHT):
        draw = Draw((-3, 3), 2)
        draw.draw_circular_light()
        draw.draw_filter(angle)
        draw.draw_plane_light(angle)
        draw.draw_lentille()
        draw.draw_free_focalization(angle)
        draw.fig.savefig(f"{dirname}/{i:04d}.png")
        del draw
        plt.close()


XSCREEN_ASTI = 1.9


def draw_asti_moving_light_screen(dirname):
    shutil.rmtree(dirname, ignore_errors=True)
    os.mkdir(dirname)
    for i, angle in enumerate(MOVE_LIGHT):
        draw = Draw((-3, 6), 2)
        draw.draw_circular_light()
        draw.draw_filter(angle)
        draw.draw_plane_light(angle)
        draw.draw_lentille()
        draw.draw_focalization_to_plane(angle, xmax=XSCREEN_ASTI)
        draw.draw_screen(angle, x=XSCREEN_ASTI)
        draw.fig.savefig(f"{dirname}/{i:04d}.png")
        del draw
        plt.close()


def draw_asti_screen(filename):
    draw = Draw((-3, 6), 2)
    draw.draw_circular_light(xmax=0)
    draw.draw_lentille()
    draw.draw_focalization_to_plane_circular(xmax=XSCREEN_ASTI)
    draw.draw_screen_circular(x=XSCREEN_ASTI)
    draw.fig.savefig(filename)


def draw_asti_moving_screen(dirname):
    shutil.rmtree(dirname, ignore_errors=True)
    os.mkdir(dirname)
    for i, screen in enumerate(MOVE_SCREEN2):
        draw = Draw((-3, 6), 2)
        draw.draw_circular_light(xmax=0)
        draw.draw_lentille()
        draw.draw_focalization_to_plane_circular(xmax=screen)
        draw.draw_screen_circular(x=screen)
        draw.fig.savefig(f"{dirname}/{i:04d}.png")
        del draw
        plt.close()


MOVE_PUPIL = np.concatenate(
    (np.linspace(0.5, 0.5 / 4, 51)[:-1], np.linspace(0.5 / 4, 0.5, 51)[:-1])
)


def draw_asti_moving_diameter(dirname):
    shutil.rmtree(dirname, ignore_errors=True)
    os.mkdir(dirname)
    for i, pupil in enumerate(MOVE_PUPIL):
        draw = Draw((-3, 6), 2)
        draw.draw_circular_light(xmax=0)
        draw.draw_lentille(radius=pupil)
        draw.draw_focalization_to_plane_circular(radius=pupil, xmax=XSCREEN_ASTI)
        draw.draw_screen_circular(radius=pupil, x=XSCREEN_ASTI)
        draw.fig.savefig(f"{dirname}/{i:04d}.png")
        del draw
        plt.close()


if __name__ == "__main__":
    _, what, where = sys.argv  # pylint: disable=unbalanced-tuple-unpacking

    match what:
        case "regular_free":
            draw_regular_free(where)
        case "regular_moving_screen":
            draw_regular_moving_screen(where)
        case "regular_moving_light_free":
            draw_regular_moving_light_free(where)
        case "asti_moving_light_free":
            draw_asti_moving_light_free(where)
        case "asti_moving_light_screen":
            draw_asti_moving_light_screen(where)
        case "asti_screen":
            draw_asti_screen(where)
        case "asti_moving_screen":
            draw_asti_moving_screen(where)
        case "asti_moving_diameter":
            draw_asti_moving_diameter(where)
