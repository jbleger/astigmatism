
.PHONY: clean mrproper all

.SECONDARY:

FIGURES = \
		  figs/filters/tldr_original_light.png \
		  figs/filters/tldr_original_dark.png \
		  figs/filters/tldr_asti_light.png \
		  figs/filters/tldr_asti_dark.png \
		  figs/optics/regular_free.png \
		  figs/optics/regular_moving_screen.webp \
		  figs/optics/regular_moving_light_free.webp \
		  figs/optics/asti_moving_light_free.webp \
		  figs/optics/asti_moving_light_screen.webp \
		  figs/optics/asti_screen.png \
		  figs/optics/asti_moving_screen.webp \
		  figs/optics/asti_moving_diameter.webp \
		  figs/filters/filter.png \
		  figs/filters/moving_filter.webp \
		  figs/filters/word_with_filter.png \
		  figs/filters/words_with_filter.png \
		  figs/filters/word_with_moving_filter.webp \
		  figs/filters/word_with_moving_diameter.webp \
		  figs/filters/final_words.png \
		  figs/filters/words.png \
		  figs/examples/codium_original.png \
		  figs/examples/codium_transformed.png \

CONTENT = \
		  content/tldr.qmd \
		  content/intro.qmd \
		  content/optics.qmd \
		  content/text.qmd \
		  content/sombre.qmd \
		  content/conclusions.qmd \
		  content/limitations.qmd \


all: index.html

clean:
	rm -rf build

mrproper: clean
	rm -rf figs

index.html: index.qmd $(CONTENT) $(FIGURES)
	quarto render index.qmd

figs/optics/%.png: optics.py
	mkdir -p figs/optics
	python3 optics.py $(subst .png,,$(subst figs/optics/,,$@)) $@

figs/optics/%.webp: optics.py
	mkdir -p figs/optics build/optics
	python3 optics.py $(subst .webp,,$(subst figs/optics/,,$@)) build/optics/$(subst .webp,,$(subst figs/optics/,,$@))
	img2webp -loop 0 -d 40 build/optics/$(subst .webp,,$(subst figs/optics/,,$@))/*.png -o $@

figs/filters/%.png: filters.py
	mkdir -p figs/filters
	python3 filters.py $(subst .png,,$(subst figs/filters/,,$@)) $@

figs/filters/%.webp: filters.py
	mkdir -p figs/filters build/filters
	python3 filters.py $(subst .webp,,$(subst figs/filters/,,$@)) build/filters/$(subst .webp,,$(subst figs/filters/,,$@))
	img2webp -loop 0 -d 40 build/filters/$(subst .webp,,$(subst figs/filters/,,$@))/*.png -o $@

figs/examples/%.png: examples.py
	mkdir -p figs/examples
	python3 examples.py $(subst .png,,$(subst figs/examples/,,$@)) $@

figs/examples/%.webp: examples.py
	mkdir -p figs/examples build/examples
	python3 examples.py $(subst .webp,,$(subst figs/examples/,,$@)) build/examples/$(subst .webp,,$(subst figs/examples/,,$@))
	img2webp -loop 0 -d 40 build/examples/$(subst .webp,,$(subst figs/examples/,,$@))/*.png -o $@
