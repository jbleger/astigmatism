import tempfile
import shutil
import subprocess
import sys

TEX_SOURCE = r"""
\documentclass[a4paper,12pt]{article}

\usepackage[french]{babel}
\usepackage[landscape,hmargin=1.5cm,vmargin=0cm]{geometry}
\usepackage{graphicx}
\usepackage{libertinus}
\usepackage[colorlinks=true,urlcolor=black]{hyperref}
\usepackage{xcolor}

\pagestyle{empty}

\begin{document}

\vspace*{\stretch{1}}

\begin{center}
\begin{tabular}{ccc}
  \begin{minipage}{.4\textwidth}
    \Large
    \begin{center}
      Vous affichez\\ \vspace{1em}
      \includegraphics[width=\textwidth]{figs/filters/tldr_original_light.png}
    \end{center}
  \end{minipage}
  &\hspace*{3cm}&
  \begin{minipage}{.4\textwidth}
    \Large
    \begin{center}
      Vous affichez\\ \vspace{1em}
      \includegraphics[width=\textwidth]{figs/filters/tldr_original_dark.png}
    \end{center}
  \end{minipage}
  \\ \\ \\ \\
  \begin{minipage}{.4\textwidth}
    \Large
    \begin{center}
      Un astigmate voit\\ \vspace{1em}
      \includegraphics[width=\textwidth]{figs/filters/tldr_asti_light.png}
    \end{center}
  \end{minipage}
  &&
  \begin{minipage}{.4\textwidth}
    \Large
    \begin{center}
      Un astigmate voit\\ \vspace{1em}
      \includegraphics[width=\textwidth]{figs/filters/tldr_asti_dark.png}
    \end{center}
  \end{minipage}
\end{tabular}
\end{center}

\vspace*{\stretch{1.25}}
\hrule

\begin{center}
\begin{tabular}{cc}
  \begin{minipage}{.75\textwidth}
    \Large
    \begin{center}
      Plus d'infos:

      \url{URL}
    \end{center}
  \end{minipage}
  &
  \begin{minipage}{.10\textwidth}
  \includegraphics[width=\textwidth]{link.pdf}
  \end{minipage}
\end{tabular}
\end{center}

\vspace*{.5cm}

\end{document}
"""

def _gen_qrcode(dirname, url):
    subprocess.run(
            (
                'qrencode',
                '-t',
                'eps',
                '-o',
                f'{dirname}/link.eps',
                '-l',
                'M',
                url))
    subprocess.run(
            (
                'epstopdf',
                f'{dirname}/link.eps',))



def _gen_latex(dirname, url):
    with open(f'{dirname}/affiche.tex','w') as f:
        f.write(TEX_SOURCE.replace('URL',url))

def _make_affiche(url):
    if not url:
        url = "https://www.example.org/"
    with tempfile.TemporaryDirectory() as d:
        _gen_qrcode(d, url)
        _gen_latex(d, url)
        shutil.copytree('figs',f'{d}/figs')
        subprocess.run(
                ('xelatex',
                 'affiche.tex'),
                cwd=d)
        shutil.copy(f'{d}/affiche.pdf', 'affiche.pdf')

if __name__ == '__main__':
    _, url = sys.argv

    _make_affiche(url)


