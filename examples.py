# pylint: disable=missing-module-docstring,missing-function-docstring

import sys
import shutil
import os

import numpy as np
import PIL.Image
import scipy.signal

import filters


def _load_image(filename, slicex=None, slicey=None):
    if slicex is None:
        slicex = slice(None, None, None)
    if slicey is None:
        slicey = slice(None, None, None)
    return np.asarray(PIL.Image.open(filename))[slicex, :][:, slicey] / 255


def _save_image(filename, data):
    PIL.Image.fromarray(np.uint8(data * 255)).save(filename)


def _apply_filter_on_each_color(image, filt, fillvalue):
    return np.array(
        [
            scipy.signal.convolve2d(image[:, :, k], filt, "same", fillvalue=fillvalue)
            for k in range(3)
        ]
    ).transpose((1, 2, 0))


HALF_PERIOD = 5


def draw_codium_original(filename):
    _save_image(
        filename,
        np.concatenate(
            [
                _load_image(f"examples/codium_{dl}.png", slice(25, None))
                for dl in ("light", "dark")
            ],
            axis=1,
        ),
    )


def draw_codium_transformed(filename):
    effects = (2, 2 * 2.5)
    filts = [filters.get_filter(effect, 0.9) for effect in effects]
    _save_image(
        filename,
        np.concatenate(
            [
                _apply_filter_on_each_color(
                    _load_image(f"examples/codium_{dl}.png", slice(25, None)),
                    filt,
                    fillvalue=(1 if dl == "light" else 0),
                )
                for dl, filt in zip(("light", "dark"), filts)
            ],
            axis=1,
        ),
    )


if __name__ == "__main__":
    _, what, where = sys.argv  # pylint: disable=unbalanced-tuple-unpacking

    match what:
        case "codium_original":
            draw_codium_original(where)
        case "codium_transformed":
            draw_codium_transformed(where)
