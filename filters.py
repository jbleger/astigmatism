# pylint: disable=invalid-name,missing-module-docstring,missing-class-docstring,missing-function-docstring,line-too-long
import shutil
import os
import sys
import itertools
import textwrap

import numpy as np
import matplotlib.pyplot as plt
import PIL.Image
import PIL.ImageFont
import PIL.ImageDraw
import scipy.signal

SAMPLING = 11011


def _block_gen(x, y, factor, angle, xscreen):
    theta = np.arctan2(y[:, None], -x[None, :])
    dist_focale = 1.75 + 0.25 * np.cos(theta + angle) ** 2
    del theta
    len_in_dir = np.abs(1.75 / (2 - 1.75) * (xscreen - dist_focale) / dist_focale)
    del dist_focale
    M2 = ((y[:, None] ** 2 + x[None, :] ** 2) < len_in_dir**2) / np.clip(
        len_in_dir, 1e-16, None
    )
    M2[y == 0][:, x == 0] = 0
    ret = M2.reshape((y.size // factor, factor, x.size // factor, factor)).mean(
        axis=(1, 3)
    )
    return ret


LIMIT = 2**18


def _get_filter_block_rec(x, y, factor, angle, xscreen):
    if x.size * y.size > LIMIT:
        if x.size > y.size:
            if x.size > factor:
                split = x.size // factor // 2 * factor
                return np.concatenate(
                    (
                        _get_filter_block_rec(x[:split], y, factor, angle, xscreen),
                        _get_filter_block_rec(x[split:], y, factor, angle, xscreen),
                    ),
                    axis=1,
                )
        else:
            if y.size > factor:
                split = y.size // factor // 2 * factor
                return np.concatenate(
                    (
                        _get_filter_block_rec(x, y[:split], factor, angle, xscreen),
                        _get_filter_block_rec(x, y[split:], factor, angle, xscreen),
                    ),
                    axis=0,
                )
    return _block_gen(x, y, factor, angle, xscreen)


def get_filter(size, alpha_screen, factor=None, angle=10 * np.pi / 180):
    xscreen = alpha_screen * 2 + (1 - alpha_screen) * 1.75

    n = 1 + 2 * int(np.ceil(size))
    if factor is None:
        if n >= SAMPLING:
            factor = 1
        else:
            factor = int((SAMPLING / n + 1) / 2) * 2 - 1

    x = np.array(
        np.arange(
            -factor * np.ceil(size)  # pylint: disable=invalid-unary-operand-type
            - factor // 2,
            factor * np.ceil(size) + 1 + factor // 2,
        )
        / (factor * size),
        dtype=np.float32,
    )
    ret = _get_filter_block_rec(x, x, factor, angle, xscreen)
    return ret / ret.sum()


class BaseImageVerySmall:  # pylint: disable=too-few-public-methods
    _image = None
    fontsize = 40

    def __new__(cls):
        if cls._image is None:
            im = PIL.Image.new("RGB", (500, 125), (255, 255, 255))
            font = PIL.ImageFont.truetype(
                "/usr/share/fonts/truetype/liberation2/LiberationSans-Regular.ttf",
                cls.fontsize,
            )
            d = PIL.ImageDraw.Draw(im)
            d.multiline_text(
                (40, 35),
                "vélocipède ou bicyclette",
                font=font,
                spacing=30,
                fill=(0, 0, 0),
            )
            cls._image = (np.asarray(im) / 255).mean(axis=2)
        return cls._image


class BaseImageSmall:  # pylint: disable=too-few-public-methods
    _image = None
    fontsize = 170

    def __new__(cls):
        if cls._image is None:
            im = PIL.Image.new("RGB", (1000, 250), (255, 255, 255))
            font = PIL.ImageFont.truetype(
                "/usr/share/fonts/truetype/liberation2/LiberationSans-Regular.ttf",
                cls.fontsize,
            )
            d = PIL.ImageDraw.Draw(im)
            d.multiline_text(
                (80, 20), "vélocipède", font=font, spacing=30, fill=(0, 0, 0)
            )
            cls._image = (np.asarray(im) / 255).mean(axis=2)
        return cls._image


class BaseImageLarge:  # pylint: disable=too-few-public-methods
    _image = None
    fontsize = 24

    def __new__(cls):
        if cls._image is None:
            text = textwrap.dedent(
                """
                Une bicyclette, ou un vélo (abréviation du mot vélocipède), est un véhicule
                terrestre à propulsion humaine entrant dans la catégorie des cycles et composé
                de deux roues alignées, qui lui donnent son nom. La force motrice est fournie
                par son conducteur (appelé « cycliste »), en position le plus souvent assise, par
                l'intermédiaire de deux pédales entraînant la roue arrière par une chaîne à rouleaux.
                """
            )[1:-1]
            im = PIL.Image.new("RGB", (1000, 250), (255, 255, 255))
            font = PIL.ImageFont.truetype(
                "/usr/share/fonts/truetype/liberation2/LiberationSans-Regular.ttf",
                cls.fontsize,
            )
            d = PIL.ImageDraw.Draw(im)
            d.multiline_text((50, 50), text, font=font, spacing=8, fill=(0, 0, 0))
            cls._image = np.array((np.asarray(im) / 255).mean(axis=2), dtype=np.float32)
        return cls._image


def _norma(image):
    return (1 / np.quantile(image, 0.95) + 1) / 2 * image


def _export_image(filename, image, normalize=True):
    if normalize:
        image = _norma(image)
    plt.imsave(
        filename,
        image,
        cmap="gray",
        vmin=0,
        vmax=1,
    )


ALPHA_HALF_PERIOD = 125  # 40 ms
MOVING_ALPHA_SCREEN = np.linspace(1, 0, ALPHA_HALF_PERIOD + 1)


def draw_filter(filename):
    filt = get_filter(250, 0.9)
    filt /= filt.max()
    filt = filt[:-1, :-1]
    plt.imsave(
        filename,
        np.concatenate((filt, filt**0.5), axis=1),
        cmap="gray",
        vmin=0,
        vmax=1,
    )


def draw_moving_filter(dirname):
    shutil.rmtree(dirname, ignore_errors=True)
    os.mkdir(dirname)
    all_filters = np.array([get_filter(250, alpha) for alpha in MOVING_ALPHA_SCREEN])
    all_filters /= all_filters.max()
    all_filters = all_filters[:, :-1, :-1]
    for i, filt in enumerate(itertools.chain(all_filters, all_filters[-2:0:-1])):
        plt.imsave(
            f"{dirname}/{i:04d}.png",
            np.concatenate((filt, filt**0.5), axis=1),
            cmap="gray",
            vmin=0,
            vmax=1,
        )


def draw_smth_with_filter(filename, smth):
    image = scipy.signal.convolve2d(
        smth(),
        get_filter(1 / 6 * smth.fontsize, 0.9),
        "same",
        fillvalue=1,
    )
    _export_image(filename, image)


def draw_smth_with_moving_filter(dirname, smth):
    shutil.rmtree(dirname, ignore_errors=True)
    os.mkdir(dirname)
    images = [
        scipy.signal.convolve2d(
            smth(), get_filter(1 / 6 * smth.fontsize, alpha), "same", fillvalue=1
        )
        for alpha in MOVING_ALPHA_SCREEN
    ]
    for i, image in enumerate(itertools.chain(images, images[-2:0:-1])):
        _export_image(f"{dirname}/{i:04d}.png", image)


MOVING_DIAMETER = np.clip(np.linspace(0, 1, 126), 1e-3, None)


def draw_smth_with_moving_diameter(dirname, smth):
    shutil.rmtree(dirname, ignore_errors=True)
    os.mkdir(dirname)
    images = [
        scipy.signal.convolve2d(
            smth(), get_filter(0.5 * smth.fontsize * x, 0.9), "same", fillvalue=1
        )
        for x in MOVING_DIAMETER
    ]
    for i, image in enumerate(itertools.chain(images, images[-2:0:-1])):
        _export_image(f"{dirname}/{i:04d}.png", image)


def _final_words(dark):
    image = scipy.signal.convolve2d(
        BaseImageLarge(),
        get_filter(1 / 6 * BaseImageLarge.fontsize * (2.5 if dark else 1), 0.9),
        "same",
        fillvalue=1,
    )
    return _norma(1 - image if dark else image)


def draw_final_words(filename):
    _export_image(
        filename, np.concatenate((_final_words(False), _final_words(True)), axis=0)
    )


def draw_tldr(filename, original, dark):
    image = BaseImageVerySmall()

    if not original:
        image = scipy.signal.convolve2d(
            image,
            get_filter(1 / 6 * BaseImageVerySmall.fontsize * (2.5 if dark else 1), 0.9),
            "same",
            fillvalue=1,
        )
    _export_image(filename, 1 - image if dark else image)


def _words(dark):
    image = BaseImageLarge()

    return _norma(1 - image if dark else image)


def draw_words(filename):
    _export_image(filename, np.concatenate((_words(False), _words(True)), axis=0))


if __name__ == "__main__":
    _, what, where = sys.argv  # pylint: disable=unbalanced-tuple-unpacking

    match what:
        case "moving_filter":
            draw_moving_filter(where)
        case "filter":
            draw_filter(where)
        case "word_with_filter":
            draw_smth_with_filter(where, BaseImageSmall)
        case "words_with_filter":
            draw_smth_with_filter(where, BaseImageLarge)
        case "word_with_moving_filter":
            draw_smth_with_moving_filter(where, BaseImageSmall)
        case "word_with_moving_diameter":
            draw_smth_with_moving_diameter(where, BaseImageSmall)
        case "final_words":
            draw_final_words(where)
        case "tldr_original_light":
            draw_tldr(where, True, False)
        case "tldr_original_dark":
            draw_tldr(where, True, True)
        case "tldr_asti_light":
            draw_tldr(where, False, False)
        case "tldr_asti_dark":
            draw_tldr(where, False, True)
        case "words":
            draw_words(where)
